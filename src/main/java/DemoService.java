

public interface DemoService {
  
    Model computeWithHttpRemoteAddrThrottling(Model model);
}
