

import com.weddini.throttling.Throttling;
import com.weddini.throttling.ThrottlingType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;


@Service
public class DemoServiceImpl implements DemoService {


    @Override
    @Throttling(limit = 2, timeUnit = TimeUnit.MINUTES)
    public Model computeWithHttpRemoteAddrThrottling(Model model) {
       return model;
    }
}
