

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Model {
    private String userName;
}
