
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.weddini.throttling.Throttling;

import java.util.concurrent.TimeUnit;


@Controller
public class ThrottledController {

    @GetMapping("/throttledController")
    @Throttling(limit = 3, timeUnit = TimeUnit.MINUTES, type = Throttling.RemoteAddr)
    public ResponseEntity<String> controllerThrottling() {
    	return ResponseEntity.ok().body("ok");
    }

}
